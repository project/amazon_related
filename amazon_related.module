<?php

/**
 * @file
 * Author: Ramiro Gómez - http://www.ramiro.org
 * A Drupal module for retrieving and displaying related products from Amazon.
 */

/**
 * Implementation of hook_menu().
 */
function amazon_related_menu() {
  $items = array();
  $items['amazon-related/%'] = array(
    'title' => 'Related Products from Amazon',
    'description' => 'Page with related poducts from Amazon for given ASIN.',
    'page callback' => 'amazon_related_page',
    'page arguments' => array(1),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK
  );
  return $items;
}


/**
 * Implementation of hook_block().
 */
//function amazon_related_block($op = 'list', $delta = 0, $edit = array()) {
//  switch ($op) {
//
//    case 'list':
//      $blocks[0]['info'] = t('Block 1');
//      $blocks[1]['info'] = t('Block 2');
//      // OPTIONAL: Add additional block descriptions here, if required.
//      return $blocks;
//
//    case 'configure':
//      // OPTIONAL: Enter form elements to add to block configuration screen, if required.
//      if ($delta == 0 && user_access('administer module')) {
//        $form['module_block_1'] = array();
//      }
//      if ($delta == 1 && user_access('administer module')) {
//        $form['module_block_2'] = array();
//      }
//      return $form;
//
//    case 'save':
//      // OPTIONAL: Add code to trigger when block configuration is saved, if required.
//      if ($delta == 0) {
//        variable_set('module_block_setting_1', $edit['module_block_1']);
//      }
//      if ($delta == 1) {
//        variable_set('module_block_setting_2', $edit['module_block_2']);
//      }
//      break;
//
//    case 'view':
//      if ($delta == 0) {
//        $block['subject'] = t('Block 1 title');
//        $block['content'] = t('Block 1 content');
//      }
//      if ($delta == 1) {
//        $block['subject'] = t('Block 2 title');
//        $block['content'] = t('Block 2 content');
//      }
//      // OPTIONAL: Enter additional cases for each additional block, if defined.
//
//      return $block;
//  }
//}

/**
 * Implementation of hook_theme().
 */
function amazon_related_theme($existing, $type, $theme, $path) {
  return array(
    'amazon_related_item' => array(
      'arguments' => array('item' => NULL)
    ),
//    'amazon_related_items_block' => array(
//      'arguments' => array('asin' => NULL)
//    )
  );
}

/**
 * Theme a product item.
 *
 * @param array $item
 * An array with product information.
 *
 * @return string $output
 * HTML output of item.
 */
function theme_amazon_related_item($item) {
  $output = '';
  // url is sanitized in l() function
  $url = $item['detailpageurl'];
  $desc = truncate_utf8(
    check_plain(strip_tags($item['editorialreviews'][0]['content'])),
    200,
    TRUE,
    TRUE
  );
  $title = check_plain($item['title']);
  $price = check_plain(
    $item['listpricecurrencycode'] . ' ' . $item['listpriceformattedprice']
  );
  $img = '<img src="' . check_url($item['imagesets']['smallimage']['url']) . '"';
  $img .= ' alt=' . $title . '" />';

  $output .= '<div class="amazon-related-item">';
  $output .= '<h3>' . l($title, $url) . '</h3>';
  $output .= '<div class="amazon-related-item-image">';
  $output .= l($img, $url, array('html' => TRUE));
  $output .= '</div>';
  $output .= '<p class="amazon-related-item-description">' . $desc;
  $output .= '<span class="amazon-related-item-price">' . $price .'</span></p>';
  $output .= '<div class="clear"></div>';
  $output .= '</div>';

  return $output;
}

/**
 * Display a page with related items for ASIN.
 *
 * @param string $asin
 * The ASIN to look up related items for.
 *
 * @return string $output
 * HTML output with related items.
 *
 * TODO override title
 */
function amazon_related_page($asin) {
  $output = '';
  drupal_add_css(drupal_get_path('module', 'amazon_related') . '/amazon_related.css');

  // override the title use first 6 words of Amazon title
  $item = amazon_item_lookup($asin);
  $title = implode(' ', array_slice(explode(' ', $item[$asin]['title']), 0, 6));
  drupal_set_title(t('@title: Related Products', array('@title' => $title)));

  $items = amazon_related_similarity_lookup($asin);
  if (count($items) > 0) {
    foreach ($items as $item) {
      $output .= theme('amazon_related_item', $item);
    }
  }
  else {
    $output = t('There are no similar items for this ASIN: @asin', array('@asin' => $asin));
  }
  return $output;
}

/**
 * Get array of related items from Amzon. Data is cached by default. Setting
 * the $force_http parameter to true forces a lookup from the Amazon Web service.
 *
 * @param string $asin
 * The ASIN to look up related items for.
 *
 * @param bool $force_http
 * Force lookup from the Amazon Web service and renew cache entry.
 *
 * @return array $items
 * An array of related items for ASIN.
 *
 * TODO make cache lifetime a setting and take care of
 * deleting outdated cache entries.
 */
function amazon_related_similarity_lookup($asin, $force_http = FALSE) {
  $items = array();
  $cid = 'amazon_related:' . $asin;
  if (!$force_http && $cache = cache_get($cid)) {
    $items = $cache->data;
  }
  else {
    $response = amazon_http_request('SimilarityLookup',
      array('ItemId' => $asin, 'ResponseGroup' => 'Large')
    );
    if (isset($response->Items)) {
      foreach ($response->Items->Item as $item) {
        $items[] = amazon_item_clean_xml($item);
      }
      if (count($items) > 0) {
        // cache at least for one day
        $lifetime = time() + 86400;
        cache_set($cid, $items, 'cache', $lifetime);
      }
    }
  }
  return $items;
}